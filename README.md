# specs_transform

transform 2d and 3d component for specs

```rust
extern crate specs;
extern crate specs_bundler;
extern crate specs_transform;


use specs::{World, DispatcherBuilder};
use specs_bundler::Bundler;
use specs_transform::{TransformBundle, Parent, Transform3D, , Transform2D};


fn main() {
    let mut world = World::new();

    let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
        .bundle(TransformBundle::<f32>::default()).unwrap()
        .build();

    let parent = world.create_entity()
        .with(Transform3D::<f32>::default())
        .build();

    let child = world.create_entity()
        .with(Parent::new(parent))
        .with(Transform2D::<f32>::default())
        .build();

    let _grandchild = world.create_entity()
        .with(Parent::new(child))
        .with(Transform3D::<f32>::default())
        .build();

    dispatcher.dispatch(&mut world.res);
}
```

#![feature(test)]

extern crate test;

extern crate specs;
extern crate specs_bundler;
extern crate specs_transform;

use test::Bencher;

use specs::{WorldExt,Builder, DispatcherBuilder, World};
use specs_bundler::Bundler;
use specs_transform::{Parent, Transform2D, Transform3D, TransformBundle};

#[bench]
fn bench_specs_transform_2d_system(b: &mut Bencher) {
    let mut world = World::new();

    let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
        .bundle(TransformBundle::<f32>::default())
        .unwrap()
        .build();

    let parent = world
        .create_entity()
        .with(Transform2D::<f32>::default())
        .build();

    let child = world
        .create_entity()
        .with(Parent::new(parent))
        .with(Transform2D::<f32>::default())
        .build();

    let _ = world
        .create_entity()
        .with(Parent::new(child))
        .with(Transform2D::<f32>::default())
        .build();

    b.iter(move || {
        dispatcher.dispatch(&mut world);
    });
}

#[bench]
fn bench_specs_transform_3d_system(b: &mut Bencher) {
    let mut world = World::new();

    let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
        .bundle(TransformBundle::<f32>::default())
        .unwrap()
        .build();

    let parent = world
        .create_entity()
        .with(Transform3D::<f32>::default())
        .build();

    let child = world
        .create_entity()
        .with(Parent::new(parent))
        .with(Transform3D::<f32>::default())
        .build();

    let _ = world
        .create_entity()
        .with(Parent::new(child))
        .with(Transform3D::<f32>::default())
        .build();

    b.iter(move || {
        dispatcher.dispatch(&mut world);
    });
}

#[bench]
fn bench_specs_transform_system(b: &mut Bencher) {
    let mut world = World::new();

    let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
        .bundle(TransformBundle::<f32>::default())
        .unwrap()
        .build();

    let parent = world
        .create_entity()
        .with(Transform3D::<f32>::default())
        .build();

    let child = world
        .create_entity()
        .with(Parent::new(parent))
        .with(Transform2D::<f32>::default())
        .build();

    let _ = world
        .create_entity()
        .with(Parent::new(child))
        .with(Transform3D::<f32>::default())
        .build();

    b.iter(move || {
        dispatcher.dispatch(&mut world);
    });
}

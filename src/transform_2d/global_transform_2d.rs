use std::ops::{Deref, DerefMut};

use mat32;
use num_traits::{One, Zero};

use specs::{Component, DenseVecStorage, FlaggedStorage};

#[derive(Debug, Serialize, Deserialize)]
pub struct GlobalTransform2D<T>(pub [T; 6]);

impl<T> Component for GlobalTransform2D<T>
where
    T: 'static + Sync + Send,
{
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

impl<T> From<[T; 6]> for GlobalTransform2D<T> {
    #[inline(always)]
    fn from(matrix: [T; 6]) -> Self {
        GlobalTransform2D(matrix)
    }
}

impl<T> Default for GlobalTransform2D<T>
where
    T: One + Zero,
{
    #[inline(always)]
    fn default() -> Self {
        Self::from(mat32::new_identity())
    }
}

impl<T> GlobalTransform2D<T>
where
    T: One + Zero,
{
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }
}

impl<T> GlobalTransform2D<T> {
    #[inline(always)]
    pub fn matrix(&self) -> &[T; 6] {
        &self.0
    }
    #[inline(always)]
    pub fn matrix_mut(&mut self) -> &mut [T; 6] {
        &mut self.0
    }
}

impl<T> AsRef<[T; 6]> for GlobalTransform2D<T> {
    #[inline(always)]
    fn as_ref(&self) -> &[T; 6] {
        self.matrix()
    }
}
impl<T> AsMut<[T; 6]> for GlobalTransform2D<T> {
    #[inline(always)]
    fn as_mut(&mut self) -> &mut [T; 6] {
        self.matrix_mut()
    }
}

impl<T> Deref for GlobalTransform2D<T> {
    type Target = [T; 6];

    #[inline(always)]
    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}
impl<T> DerefMut for GlobalTransform2D<T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.as_mut()
    }
}

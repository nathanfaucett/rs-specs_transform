use specs::prelude::{Component, DenseVecStorage, Entity, FlaggedStorage};
use specs_scene_graph::{Parent as SceneGraphParent, SceneGraph};

pub type TransformSceneGraph = SceneGraph<Parent>;

#[derive(Debug, Clone, Eq, Ord, PartialEq, PartialOrd)]
pub struct Parent {
    pub entity: Entity,
}

impl Component for Parent {
    type Storage = FlaggedStorage<Self, DenseVecStorage<Self>>;
}

impl Parent {
    #[inline(always)]
    pub fn new(entity: Entity) -> Self {
        Parent { entity: entity }
    }
}

impl SceneGraphParent for Parent {
    #[inline(always)]
    fn parent_entity(&self) -> Entity {
        self.entity
    }
}
